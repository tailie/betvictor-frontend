export default {
	// Meta
	'_id': 1,
	'_language_name': 'Spanish',
	'_flag': 'es',

	// Translations
	'Sports': 'Los Deportes',
	'Sport Browser': 'Navegador Deportivo',
	'Football': 'Futbol',
	'Handball': 'Balonmano',
	'Badminton': 'Bádminton',

	'Tennis': 'Tenis',
	'Basketball': 'Baloncesto',
	'Ice Hockey': 'Hockey sobre hielo',
	'Golf': 'Golf',
	'Cricket': 'Grillo',
	'Beach Volleyball': 'Voleibol de playa',
	'Table Tennis': 'Mesa de tennis',
	'Winter Sports': 'Deportes de invierno',

	'sports': 'los deportes',
	'Football events': 'Eventos de futbol',
	'Tennis events': 'Eventos de tenis',
	'Basketball events': 'Eventos de baloncesto',
	'Ice Hockey events': 'Eventos de hockey sobre hielo',
	'Golf events': 'Eventos de golf',
	'Cricket events': 'Eventos de grillo',
	'Snooker events': 'Eventos de snooker',
	'Beach volleyball events': 'Eventos de voley playa',
	'Table Tennis events': 'Eventos de mesa tennis',
	'Winter Sport events': 'Eventos de deportes de invierno',
	'Badminton events': 'Eventos de bádminton',
	'Handball events': 'Eventos de balonmano',

	'Match Betting': 'Apuestas de partidos',
	'Draw' : 'Emparejar',

	'events': 'eventos',
	'Back to': 'Volver a',
	'started {num} minutes ago': 'comenzó hace {num} minutos',
	'started 1 minute ago': 'comenzó hace 1 minuto'

};
