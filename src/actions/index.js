import { dispatch } from '../modules/store';

export const setData = data => dispatch({ type: 'SET_DATA', data });

export const setLanguage = language => dispatch({ type: 'SET_LANGUAGE', language });
