export default {
	project_name: 'Sport Browser',
	namespace: 'sport_browser',
	api_url: (
		process.env.API_ENDPOINT === 'staging' ? (
			''
		) : process.env.API_ENDPOINT === 'local' ? (
			'http://localhost:4567'
		) : process.env.NODE_ENV === 'development' ? (
			'http://localhost:4567'
		) : (
			'http://localhost:4567'
		)
	)
};
