import _ from 'lodash';
import languageEN from '../languages/en.js';
import languageES from '../languages/es.js';

import persistence from './persistence';

const translations = {
	'EN': languageEN,
	'ES': languageES
};

function isLanguageSupported(language) {
	return language.substr(0, 2).toUpperCase() in translations;
}

let translateFnCache = {};

export function getInitialLanguage() {
	const savedLanguage = persistence.get('language');
	if (savedLanguage !== null && isLanguageSupported(savedLanguage)) {
		return savedLanguage;
	}

	//Try to detect browser language
	if (window.navigator.languages !== undefined) {
		const result = _.find(window.navigator.languages, language => isLanguageSupported(language));
		if (result !== undefined) {
			return result.substr(0, 2).toUpperCase();
		}
	}
	return 'EN';
}

export function translateFn(language) {
	if (language in translateFnCache) {
		return translateFnCache[language];
	}

	const fn = function(key) {
		if (!(key in translations[language])) {
			return key;
		}
		return translations[language][key];
	};
	translateFnCache[language] = fn;
	return fn;
}

export function getSupportedLanguages() {
	return _.map(translations, (value, key) => {
		return { locale: key, flag: value._flag, name: value._language_name };
	});
}
