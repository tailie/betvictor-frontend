import { createStore, combineReducers, applyMiddleware } from 'redux';

import reducers from '../reducers/index';

export const store = createStore(
	combineReducers(reducers),
	{},
	applyMiddleware(() => next => action => next(action)),

);

export const dispatch = store.dispatch;