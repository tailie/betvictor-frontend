import config from '../config';

function createAjaxReq(obj, method, resolve, reject) {
	const request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (this.readyState !== 4) {
			return;
		}

		const { status, responseText } = this;

		let parsedJSON = null;
		let success = status >= 200 && status <= 299;
		if (responseText.length !== 0) {
			try {
				parsedJSON = JSON.parse(responseText);
			} catch (e) {
				parsedJSON = null;
				success = false;
			}
		}

		if (!success){
			// reject({status: status, 'error': true})
		}

		if (success) {
				resolve(parsedJSON);
		} else {
				reject({status: status, 'error': true}); //todo get error message from api
		}
	};
	return request;
}

export function ajaxGET(obj) {
	return new Promise((resolve, reject) => {

		const request = createAjaxReq(obj, 'GET', resolve, reject);

		let { data } = obj;

		const params = [];
		if (data !== undefined) {
			for (var key in data) {
				params.push(key + '=' + encodeURIComponent(data[key]));
			}
		}

		request.open('GET', (obj.api ? config.api_url + obj.api : obj.url) + (params.length ?  '?' + params.join('&') : ''), true);
		request.send();
	});
}

//Don't need to implement this part for the demo
export function ajaxPOST(obj) {
	// todo
}
