import { translateFn, getInitialLanguage } from '../modules/i18n';
import persistence from '../modules/persistence';

const initialLanguage = getInitialLanguage();
const initialFn = translateFn(initialLanguage);

export function i18n(state = initialFn, action) {
	switch (action.type) {
		case 'SET_LANGUAGE':
			persistence.set('language', action.language);
			return translateFn(action.language);
		default:
			return state;
	}
}

export function language(state = initialLanguage, action) {
	switch (action.type) {
		case 'SET_LANGUAGE':
			return action.language;
		default:
			return state;
	}
}
