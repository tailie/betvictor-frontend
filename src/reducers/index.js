import data from './data';
import { i18n, language } from './language';

export default {
	data,
	i18n,
	language,
};
