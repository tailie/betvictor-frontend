export function unixToLocaleDate(timestamp) {
	const locale = 'sv-SE' // todo make respond to locale language setting

	return {
		dateString: new Date(timestamp).toLocaleDateString(locale),
		dayString: new Date(timestamp).toLocaleDateString(locale, {
			weekday: 'long'
		}),
		timeString: new Date(timestamp).toLocaleDateString(locale, {
			hour: '2-digit',
			minute: '2-digit',
		})
	};
}

export function timeLeft(timestamp) {
	var startTime = new Date();
	var endTime = new Date(timestamp);
	return endTime.getTime() - startTime.getTime();
}

export function timeLeftString(timestamp) {
	const difference = timeLeft(timestamp);

	const diff = {
		min: Math.round(difference / 60000),
		hours: Math.round(difference / (60000*60)),
		days: Math.round(difference / (60000*60*24))
	};

	return Math.abs(diff.min);
}