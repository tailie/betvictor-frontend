import React, { Component } from 'react';
import { connect } from 'react-redux';

import FlagBar from './containers/FlagBar/FlagBar';

export class Header extends Component {
	render() {
		const { i18n } = this.props;
		return (
			<header className="header">
				<div className="stack stack--middle">
					<div className="stack__item full">
						<h1 className="header__title">{i18n("Sport Browser")}</h1>
					</div>
					<div className="stack__item">
						<FlagBar />
					</div>
				</div>
			</header>
		)
	}
}

export default connect(state => ({
	i18n: state.i18n,
}))(Header);