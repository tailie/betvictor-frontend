import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

class HomeTemplate extends PureComponent {
	render() {
		const { data, i18n } = this.props;

		return (
			<div className="">
				{!data ? (<p>Loading...</p>) : (
					<div>
						<h2>{i18n('Sports')}</h2>
						<div className="grid">
							{data.map((sport, i) => (
								<div key={i} className="grid__item one-fourth laptop--one-third mobile--one-half">
									<div className="block">
										<Link to={`/sports/${sport.id}`}>
											<div className="block__content">
												<h3 className="block__title" key={i}>{i18n(sport.name)}</h3>
												<p>{sport.events.length} {i18n('events')}</p>
											</div>
										</Link>
									</div>
								</div>
							))}
						</div>
					</div>
				)}
			</div>
		)
	}
}

export default connect(state => ({
	data: state.data,
	i18n: state.i18n,
}))(HomeTemplate);