import React, { Component } from 'react';

import { setData } from '../../actions';
import { ajaxGET } from '../../modules/ajax';

import HomeTemplate from './Home.jsx';
class Home extends Component {
	componentDidMount() {
		this.fetchInterval = setInterval(this.fetch.bind(this), 4000);
		this.fetch();
	}
	
	fetch() {
		ajaxGET({
			api: '/sports',
		}).then(response => {
			setData(response);
		}).catch(error => {
			alert('error occurred! ' + JSON.stringify(error));
		})
	}

	componentWillUnmount() {
		clearInterval(this.fetchInterval);
	}

	render() {
		return (
			<HomeTemplate />
		);

	}
}

export default Home;