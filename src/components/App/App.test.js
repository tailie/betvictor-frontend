import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { setData } from '../../actions/';
import { expect } from 'chai';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('should set the data in redux store', function() {
  const expectedAction = {
    type: 'SET_DATA',
    data: { id: 100 }
  };

  expect(setData({id: 100}).data.id).to.equal(expectedAction.data.id);
});

