import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';

import { store } from '../../modules/store';
import { ajaxGET } from '../../modules/ajax';
import { setData } from '../../actions';

import AppTemplate from './App.jsx';

class App extends Component {
  componentDidMount() {
    this.fetchInterval = setInterval(this.serverFetch.bind(this), 4000);
    this.serverFetch();
  }

  componentWillUnmount() {
    clearInterval(this.fetchInterval);
  }

  serverFetch() {
      ajaxGET({
        api: '/'
      }).then(response => {
        setData(response);
      }).catch(error => {
        alert('error occurred! ' + JSON.stringify(error));
      });
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <AppTemplate />
        </Router>
      </Provider>
    );
  }
}

export default App;

  
  