import React from 'react';

import './App.css';

import ContentWrapper from '../ContentWrapper';
import Footer from '../Footer';
import Header from '../Header';

export default () => (
	<div id="app">
		<Header />
		<div className="content">
			<ContentWrapper />
		</div>
		<Footer />
	</div>
);