import React, { Component } from 'react';
import { connect } from "react-redux";

import SportsSingleTemplate from './SportsSingle.jsx';

import { ajaxGET } from '../../modules/ajax';

export class SportsSingle extends Component {
	state = {
		sport: null
	};

	componentDidMount() {
		this.fetchInterval = setInterval(this.fetch.bind(this), 4000);
		this.fetch();
	}

	componentWillUnmount() {
		clearInterval(this.fetchInterval);
	}

	fetch() {
		const { match } = this.props;
		const sport_id = parseInt(match.params.sport_id, 10);

		ajaxGET({
			api: `/sports/${sport_id}`,
		}).then(response => {
			this.setState({sport: response});
		}).catch(error => {
			alert('error occurred! ' + JSON.stringify(error));
		})
	}

	render() {
		const { sport } = this.state;

		/**
		 * In case of a web app reading and updating from a single endpoint, this
		 * could be the extract events method for the Sports frontend collection.
		 *
		 */

		// const id = parseInt(match.params.sport_id, 10);
		// const sport = data ? _.find(data, { id:id }) : null;

		/*
		const events = sport ? _.flatMap(sport.comp,
			(c) => c.events.map(ev => {
					return {
						url: `/sports/${sport.id}/events/${ev.id}`,
						comp: c.desc,
						...ev
					};
			})).sort((a, b) => new Date(b.time) - new Date(a.time)) : null;
		*/

		return (
			<SportsSingleTemplate sport={sport}/>
		);

	}
}

export default connect(state => ({
	data: state.data,
	i18n: state.i18n
}))(SportsSingle);