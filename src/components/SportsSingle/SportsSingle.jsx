import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

import EventRow from '../containers/EventRow';

export class SportsSingle extends PureComponent {
	render() {
		const { sport, i18n } = this.props;

		return (
			<div className="">
				{!sport ? (<p>Loading...</p>) : (
					<div>
						<h2>{i18n(`${sport.name} events`)}</h2>
						<p>
							<Link to="/">
								{i18n("Back to")} {i18n("sports")}
							</Link>
						</p>
						<div className="list">
							{sport.events ? sport.events.map((event, i) => (
								<div className="list__item" key={i}>
									<EventRow event={event} key={i}/>
								</div>
							)) : <p>{i18n("No events for this sport")}</p>}
						</div>
					</div>
				)}
			</div>
		)
	}
}

export default connect(state => ({
	i18n: state.i18n,
}))(SportsSingle);