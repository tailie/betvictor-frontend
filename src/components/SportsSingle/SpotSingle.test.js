import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';

import { SportsSingle } from './SportsSingle.js';
import _SportsSingleTemplate, { SportsSingle as SportsSingleTemplate } from './SportsSingle.jsx';
import _EventRow from '../containers/EventRow';

import {setData } from '../../actions/';

import mockData from '../../data/mockStore';

const mockMatch = {
	params: {
		sport_id: 100,
		event_id: 239
	}
};

const mockSport = mockData[0];

/* Template test */
describe('<SportsSingleTemplate />', () => {
	const props = {
		sport: mockSport,
		i18n: text => text
	};

	it('renders two <EventRow /> components', function() {
		const wrapper = shallow(<SportsSingleTemplate {...props} />, props);
		expect(wrapper.find(_EventRow)).to.have.lengthOf(23);
	});
});

/* Component test */
describe('<SportsSingle />', () => {
	it('renders one <SportsSingleTemplate /> components', function() {
		const wrapper = shallow(<SportsSingle match={mockMatch} />);
		expect(wrapper.find(_SportsSingleTemplate)).to.have.lengthOf(1);
	});
});
