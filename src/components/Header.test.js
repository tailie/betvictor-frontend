import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';

import { Header }  from './Header';
import _FlagBar from './containers/FlagBar/FlagBar';


const props = {
	i18n: text => text,
	language: "EN"
};

describe('<Header />', () => {
	it('renders one <FlagBar /> component', () => {
		const wrapper = shallow(<Header {...props} />);
		expect(wrapper.find(_FlagBar)).to.have.lengthOf(1);
	});
});