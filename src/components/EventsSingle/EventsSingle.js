import React, { Component } from 'react';
import { connect } from "react-redux";
import _  from 'lodash';

import { ajaxGET } from '../../modules/ajax';


import EventsSingleTemplate from './EventsSingle.jsx';
export class EventsSingle extends Component {
	state = {
		event: null
	};

	componentDidMount() {
		this.fetchInterval = setInterval(this.fetch.bind(this), 4000);
		this.fetch();
	}

	componentWillUnmount() {
		clearInterval(this.fetchInterval);
	}

	fetch() {
		const { match } = this.props;
		const event_id = parseInt(match.params.event_id, 10);
		const sport_id = parseInt(match.params.sport_id, 10);

		ajaxGET({
			api: `/sports/${sport_id}/events/${event_id}`,
		}).then(response => {
			this.setState({event: response});
		}).catch(error => {
			alert('error occurred! ' + JSON.stringify(error));
		});
	}

	render() {
		const { data, match } = this.props;

		const { event } = this.state;

		const sport_id = parseInt(match.params.sport_id, 10);
		const sport = data ? _.find(data, { id:sport_id }) : null;

		/*
		let event = null;

		if(data) {
			const events = data.map(d => d.comp).flat();
			const events2 = data ? _.flatMap(events,
				(c) => c.events.map(ev => {
					return {
						comp: c.desc,
						...ev
					};
				})) : null;


			event = _.find(events2, {id: id});
		}
		*/
		return (
			<EventsSingleTemplate event={event} sport={sport} />
		);

	}
}

export default connect(state => ({
	i18n: state.i18n,
	data: state.data,
}))(EventsSingle);