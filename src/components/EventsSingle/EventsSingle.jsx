import React, { PureComponent } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

import { unixToLocaleDate } from '../../tools/date';

import Scoreboard from '../containers/Scoreboard/Scoreboard';

export class EventsSingle extends PureComponent {
	render() {
		const { event, sport, i18n } = this.props;

		return (
			<div className="events-single">
				{!event ? (<p>Loading...</p>) : (
					<div>
						<h2>{sport ? i18n(`${sport.name} events`) : ''} - {event.name}</h2>

						<p>
							{sport && (
								<Link to={`/sports/${sport.id}`}>
									{i18n("Back to")} {i18n(sport.name)}
								</Link>
							)}
						</p>
						<div className="grid">
							<div className="grid__item one-half laptop--full">
								<div className="block block--white block--inset">
									<h4>{event.comp}</h4>
									<h4>{i18n('Starts at')} {unixToLocaleDate(event.time).timeString}</h4>
								</div>
							</div>
							<div className="grid__item one-half laptop--full">
								<div className="block block--white block--inset">
									<Scoreboard event={event} />
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		)
	}
}

export default connect(state => ({
	i18n: state.i18n,
}))(EventsSingle);