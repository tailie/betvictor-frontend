import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

import { EventsSingle } from './EventsSingle.js';
import { EventsSingle as EventsSingleTemplate }   from './EventsSingle.jsx';
import _Scoreboard from '../containers/Scoreboard/Scoreboard';

import mockData from '../../data/mockStore';

const mockMatch = {
		params: {
			sport_id: 100, 
			event_id: 239
		}
};

const mockSport = mockData[0];
const mockEvent = mockData[0].events[0];

/* Template test */
describe('<EventsSingleTemplate />', function() {
	const templateProps = {
		sport: mockSport,
		event: mockEvent,
		i18n: text => text
	};

	it('should mount in a full DOM', function() {
		const wrapper = shallow(<EventsSingleTemplate {...templateProps} />);
		expect(wrapper.find('.events-single')).to.have.lengthOf(1);
	});

	it('should have the proper generated h2', function() {
		const wrapper = shallow(<EventsSingleTemplate {...templateProps} />);
		expect(wrapper.find('h2').text()).to.equal(mockData[0].name + " events - " + mockEvent.name);
	});

	it('should render Scoreboard', function() {
		const wrapper = shallow(<EventsSingleTemplate {...templateProps} />);
		expect(wrapper.find(_Scoreboard)).to.have.lengthOf(1);

	});
	
});

/* Component test */
describe('<EventsSingle />', function() {
	const props = {
		match: mockMatch,
		data: mockData,
		sport: mockSport,
		event: mockEvent,
		i18n: text => text
	};

	it('calls componentDidMount', () => {
		sinon.spy(EventsSingle.prototype, 'componentDidMount');
		const wrapper = shallow(<EventsSingle {...props} />, props);
		expect(EventsSingle.prototype.componentDidMount).to.have.property('callCount', 1);
	});
});