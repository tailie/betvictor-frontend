import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import Home from './Home/Home';
import Sports from './Sports';
import SportsSingle from './SportsSingle/SportsSingle';
import Events from './Events';
import EventsSingle from './EventsSingle/EventsSingle';

const ContentWrapper = props => (
	<React.Fragment>
		<Switch>
			<Route path="/" exact component={Home} />
			<Route path="/sports" exact component={Sports} />
			<Route path="/sports/:sport_id" exact component={SportsSingle} />
			<Route path="/sports/:sport_id/events" exact component={Events} />
			<Route path="/sports/:sport_id/events/:event_id" exact component={EventsSingle} />
		</Switch>
	</React.Fragment>
);
export default withRouter(ContentWrapper);
