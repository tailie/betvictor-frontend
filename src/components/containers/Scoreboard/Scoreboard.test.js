import React from 'react';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';
import _ from 'lodash';

import { setLanguage } from '../../../actions/';

import { Scoreboard } from './Scoreboard.js';

import mockData from '../../../data/mockStore';

const props = {
	event: mockData[0]['events'][0],
	i18n: text => text
}

const market = props.event.markets[0];
market.o = _.orderBy(market.o, 'po');

describe('<Scoreboard />', function() {
	it('Render team titles correctly', () => {
		const wrapper = mount(<Scoreboard {...props} />);
		expect(wrapper.find('.scoreboard__title').at(0).text()).to.equal(props.event.oppADesc);
		expect(wrapper.find('.scoreboard__title').at(1).text()).to.equal(props.event.oppBDesc);
	});

	it('Render team scores correctly', () => {
		const wrapper = mount(<Scoreboard {...props} />);
		expect(wrapper.find('.scoreboard__score').at(0).text()).to.equal(props.event.scoreboard.scrA.toString());
		expect(wrapper.find('.scoreboard__score').at(1).text()).to.equal(props.event.scoreboard.scrB.toString());
	});

	it('Render betting market title correctly', () => {
		const wrapper = mount(<Scoreboard {...props} />);
		expect(wrapper.find('.market__title').at(0).text()).to.equal(market.desc);

	});

	it('Render betting scores correctly', () => {
		const wrapper = mount(<Scoreboard {...props} />);

		// Test all Betting values compared to values in mock data
		market.o.forEach((bettingValue, i) => {
			expect(wrapper.find('.market').at(0).find('.market__score').at(i)
				.find('p').text()).to.equal(bettingValue.pr);
		});
	});
});