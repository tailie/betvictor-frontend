import React, { Component } from 'react';
import { connect } from "react-redux";
import _  from 'lodash';

export class Scoreboard extends Component {
	render () {
		const { event, i18n } = this.props;

		return (
			<React.Fragment>
				{event.scoreboard && !event.scoreboard.disabled && (
					<div className="scoreboard">
						<div className="row">
							<div className="col one-half">
								<div className="scoreboard__title">
									{event.oppADesc}
								</div>
								<p className="scoreboard__score">
									{event.scoreboard.scrA}
								</p>
							</div>
							<div className="col one-half">
								<div className="scoreboard__title">
									{event.oppBDesc}
								</div>
								<p className="scoreboard__score">
									{event.scoreboard.scrB}
								</p>
							</div>

						</div>
						{event.markets && event.markets.map((market, i) => (
							<div className="market" key={i}>
								<p className="market__title">{i18n(market.desc)}</p>
								<div className="stack stack--fixed">
									{_.orderBy(market.o, 'po').map((o, i2) => (
										<div className="stack__item market__score" key={i2}>
											<h3>{o.d === 'Draw' ? i18n(o.d) : o.d}</h3>
											<p>{o.pr}</p>
										</div>

									))}

								</div>
							</div>
						))}
					</div>
				)}
			</React.Fragment>
		)
	}
}

export default connect(state => ({
	i18n: state.i18n,
}))(Scoreboard);