import React from 'react';
import { shallow, mount, render } from 'enzyme';

import { FlagBar } from './FlagBar.js';

import { setLanguage } from '../../../actions/';

import { expect } from 'chai';

describe('<FlagBar />', function() {
	it('check Language Selection dropdown open and close', () => {
		const wrapper = mount(<FlagBar language="EN" />);
		const button = wrapper.find("button");
		expect(wrapper.find('.dropdown').hasClass('dropdown--show')).to.equal(false);
		button.simulate('click');
		expect(wrapper.find('.dropdown').hasClass('dropdown--show')).to.equal(true);
	});

	it('should set language in redux store', function() {
		const expectedAction = {
			type: 'SET_LANGUAGE',
			language : "EN"
		};
		expect(setLanguage("EN").language).to.equal(expectedAction.language);
	});
});