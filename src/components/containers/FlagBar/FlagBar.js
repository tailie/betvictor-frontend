import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import { setLanguage } from '../../../actions/';
import { getSupportedLanguages } from '../../../modules/i18n';

const supportedLanguages = getSupportedLanguages();

export class FlagBar extends React.Component {
	state = {
		show: false,
	};

	handleSetLanguage = locale => {
		setLanguage(locale);
		this.setState({show: false});
	};

	render() {
		const { show } = this.state;
		const { language } = this.props;
		
		const selectedLanguage = _.find(supportedLanguages, { locale: language });

		return (
		<div className={"dropdown"  + (show ? ' dropdown--show' : '')}>
			<button onClick={() => this.setState({ show: !this.state.show })} className="button">
				{selectedLanguage.name}
				<span className={`indent-left famfamfam-flag famfamfam-flag-${selectedLanguage.flag}`}></span>

			</button>
			<div className="dropdown__content">
				{supportedLanguages.map((f, i) => (
				<div className="dropdown__content-button" key={i} onClick={() => this.handleSetLanguage(f.locale)}>
					{f.name} <span className={`indent-left famfamfam-flag famfamfam-flag-${f.flag}`}></span>
				</div>
				))}
			</div>
		</div>
		);
	}
}

export default connect(state => ({
	i18n: state.i18n,
	language: state.language,
}))(FlagBar);
