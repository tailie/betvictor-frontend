import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';

import { unixToLocaleDate, timeLeftString, timeLeft } from '../../tools/date';

class EventRow extends Component {
	render() {
		const { event, i18n } = this.props;

		return (
			<Link to={event.url} className="stack event-row">
				<div className="stack__item full event-row__block">
					<h3 className="event-row__block-title">{event.desc}</h3>
					<h4 className="event-row__block-subtitle">{event.comp}</h4>
				</div>
				<div className="stack__item mobile-full mobile-block">
					<p>{unixToLocaleDate(event.time).timeString}</p>
					<p className="oneliner">
						{timeLeft(event.time) > 0
							? i18n(`begins in {num} minutes`).replace('{num}', timeLeftString(event.time))
							: i18n(`started {num} minutes ago`).replace('{num}', timeLeftString(event.time))
						}
					</p>
				</div>
			</Link>

		);

	}
}

export default connect(state => ({
	i18n: state.i18n,
}))(EventRow);